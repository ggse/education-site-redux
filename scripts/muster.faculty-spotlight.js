/*!
 * The Gevirtz School Faculty Spotlight Page
 * http://education.ucsb.edu
 *
 * Copyright (c) 2014 Justin Force, Zhenya Frolov, Sean Nguyen, and UC Regents
 * Licensed under the BSD 3-Clause License
 */

/*jslint browser: true, indent: 2 */
/*global jQuery, muster */

/*
 *Output:
 *  _____________ ________________
 * |*Instructor  |First 25 words  |
 * |Image*       |of instructor's |
 * |             |biography       |
 * |             |                |
 * |             |                |
 *  -------------                 |
 * |Instructor   |  >Link to their|
 * |Name         |   bio page     |
 *  ------------------------------
 *
 */
(function($) {

	'use strict';

	var $facultySpotlight;
	var temp = "Academic Senate Faculty";
	$(document).ready(muster('ggsedb').query({
		select: 'active, first_name, last_name, faculty_listing_category, biography',
		from: 'profile',
		where: "active = 'yes' and faculty_listing_category = 'Academic Senate Faculty'"
	}, function() {

		var randomIndex, person, $spotlightContent, imgsrc;
		randomIndex = Math.floor(this.results.length * Math.random());
		person = this.results[randomIndex];
		imgsrc = '/drupal7/sites/default/files/faculty_photos/' + (person.first_name + person.last_name).toLowerCase().replace(/[^a-z]/g, '') + '.jpg';
		//helper function
		function checkImage(imgurl){
			function headRequest(url){
				var http = $.ajax({
					url: url,
					type: "HEAD",
					async: false
				})
				return http.status;
			}
			if(headRequest(imgurl) == 200) {
				return true;
			}
			else {
				return false;
				console.log(imgsrc + 'not found');
			}
		}
		//if image does not exist or person.biography is undefined, reroll
		while (!checkImage(imgsrc) || person.biography == undefined) {
			randomIndex = Math.floor(this.results.length * Math.random());
			person = this.results[randomIndex];
			imgsrc = '/drupal7/sites/default/files/faculty_photos/' + (person.first_name + person.last_name).toLowerCase().replace(/[^a-z]/g, '') + '.jpg';
		}

		var link = "/research-faculty/bio?first=" + person.first_name + "&last=" + person.last_name;
		var cutBio = person.biography.slice(0, 150).replace(/\s+\S*$/, "");

		$spotlightContent = $('<div>');
		$spotlightContent.append($('<img src="https://education.ucsb.edu/drupal7/sites/default/files/faculty_photos/' + (person.first_name + person.last_name).toLowerCase().replace(/[^a-z]/g, '') + '.jpg"/>'));
		$spotlightContent.append($('' + cutBio + '...<p><a href="' + link + '"><b>...more&raquo;</b></a></p>'));

		$(function() {
			if ($facultySpotlight === undefined) {
				$facultySpotlight = $('#facultySpotlight');
			}
			$facultySpotlight.append($spotlightContent);
		});

	}))
}(jQuery));